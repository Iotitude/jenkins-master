FROM jenkinsci/jenkins:2.70

USER root
RUN mkdir /var/log/jenkins && \
mkdir /var/cache/jenkins

RUN chown -R jenkins:jenkins /var/log/jenkins && \
chown -R jenkins:jenkins /var/cache/jenkins

RUN apt-get update && apt-get install -y sudo

USER jenkins
RUN /usr/local/bin/install-plugins.sh git matrix-auth workflow-aggregator ssh-slaves gitlab-plugin envinject robot

ENV JAVA_OPTS -Xmx8192m -Djenkins.install.runSetupWizard=false 
